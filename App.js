import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {createAppContainer,createSwitchNavigator} from 'react-navigation';

import LoginScreen from './screens/LoginScreen.js'
import LoadingScreen from './screens/LoadingScreen.js'
import ProfileScreen from './screens/ProfileScreen.js'
import HomeScreen from './screens/HomeScreen.js'


import ApiKeys from './constants/ApiKeys.js'
import * as firebase from 'firebase';




if (!firebase.apps.length) {
    firebase.initializeApp(ApiKeys.firebaseConfig);
}


class App extends React.Component{
  render(){
    return <AppNavigator/>;
  }
}

const AppSwitchNavigator = createSwitchNavigator({
  LoadingScreen: LoadingScreen,
  LoginScreen: LoginScreen,
  ProfileScreen: ProfileScreen,
  HomeScreen: HomeScreen

})

const AppNavigator = createAppContainer(AppSwitchNavigator);



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  }
});
export default AppNavigator;
