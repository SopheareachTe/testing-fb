import React from 'react';
import { StyleSheet, Text, View,Image,ScrollView,Button,TouchableOpacity,Alert } from 'react-native';
import { Icon,Card,Body, CardItem} from 'native-base';
import { WebView } from 'react-native-webview';
import * as firebase from 'firebase';
import {createAppContainer} from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import * as SecureStore from 'expo-secure-store';
import Ionicons from 'react-native-vector-icons/Ionicons';



class ShopScreen extends React.Component{
  render(){
    return (
      <View style={styles.container}>
        <Text>Shop</Text>
      </View>
    )
  }
}




class LiveScreen extends React.Component{

  constructor() {
       super()
       this.state = {
           name:'',
           pageName:'',
           liveURL:'',
           photoPageURL:'./../assets/login.jpg',
   };
 }

 componentDidMount(){
   this.read();
 }

 read = async () => {
 try {
   const id = await SecureStore.getItemAsync('id');

   if (id) {
     const myJson = JSON.parse(id);
     firebase
     .database()
       .ref()
       .child('livestream')
       .orderByChild('name')
       .equalTo(myJson.name)
       .on('value', snapshot => {
           const livestream = snapshot.val();
           if(livestream!=null){
           this.setState({
             name:livestream[myJson.name].name,
             pageName:livestream[myJson.name].pageName,
             liveURL:livestream[myJson.name].liveURL,
             photoPageURL:livestream[myJson.name].photoURL,
           });
         }
       });
   }
 } catch (e) {
   console.log(e);
 }
 };

endLivestream = async () => {
  firebase
  .database()
    .ref()
    .child('/livestream/'+this.state.name)
    .remove();
    this.setState({
      name:'',
      pageName:'',
      liveURL:'',
      photoPageURL:'./../assets/login.jpg',
    });
}


  render(){
    if(this.state.liveURL==''){
      return (
        <View style={styles.container}>
          <Text>Please connect go to profile to connect a livestream</Text>
        </View>
      )
    }
    else{
      return (
        <Body style={{ flex: 1, flexDirection: 'row' }}>
        <WebView
          source={{html: '<div style="display:flex;position:absolute;top:25;left:25;width:100%;height:40px;"><img src="'+this.state.photoPageURL+'" style="border-radius: 50%;" height="100" width="100"/><h1 style="font-size:60px;margin-left:20px;margin-top:20px;">'+this.state.pageName+'</h1></div><iframe src="'+this.state.liveURL+'" width="100%" height="100%" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="false" marginheight="0" marginwidth="0" ></iframe>'}}
          style={{marginTop: 20}}
          userAgent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
          mediaPlaybackRequiresUserAction={false}
          originWhitelist={['*']}
          scalesPageToFit={true}
          allowsInlineMediaPlayback={true}
        />
        <TouchableOpacity  style={styles.liveButtonStyle}
        onPress={()=>{this.endLivestream()}}>
          <Text
          style={styles.textStyle}
          >End</Text>
      </TouchableOpacity>
        </Body>
      )}
  }
}




class ProfileScreen extends React.Component{
  constructor() {
       super()
       this.state = {
           name: '',
           liveURL:'',
           photoURL:'./../assets/login.jpg',
           photoPageURL:'./../assets/login.jpg',
           token:'',
           data:  {"accounts":
             {"data":[
               {"name":"None","id":'1'}],
               "paging":{"cursors":{"before":"MTAyMTkxNDg3OTkwMTU1","after":"MTY3NTM4MjgwNjM3NTE2"}}},"id":"2738072386270127"},
       };
   }

   componentDidMount(){
            this.read();
  }


    callGraph = async token => {

      const response = await fetch(
        `https://graph.facebook.com/v6.0/me?access_token=${token}&fields=accounts`
      );
      const responseJSON = await response.json();
      for(x in responseJSON.accounts.data){
        const responseURL = await fetch(
          `https://graph.facebook.com/v6.0/${responseJSON.accounts.data[x].id}/picture?redirect=0&type=large`
        );
        const responseURLJSON = await responseURL.json();
        responseJSON.accounts.data[x].photoURL = responseURLJSON.data.url;
      }
      this.setState({data:responseJSON,});
    };

    getLive = async (token,t) =>{
      const response = await fetch(
        `https://graph.facebook.com/v6.0/${t.id}/live_videos?access_token=${token}`
      );
      const responseJSON = await response.json();
      var embed;
      for(x in responseJSON.data){
        if(responseJSON.data[x].status=='VOD'){
          embed = responseJSON.data[x].embed_html;
          embed = embed.replace('<iframe src="','');
          embed = embed.slice(0,136);
          embed = embed.slice(0,43)+'autoplay=true&allowfullscreen=false&'+embed.slice(43,136)
          console.log(embed);
          this.setState({liveURL: embed});
        }
      }

      if(this.state.liveURL!=''){
      Alert.alert('Livestream connected!');
      firebase
        .database()
        .ref('/livestream/'+this.state.name)
        .set({
          name:this.state.name,
          pageName:t.name,
          liveURL: this.state.liveURL,
          photoURL: t.photoURL,
        })
      }
      else{
        Alert.alert('Make sure you already start livestream in facebook!');
      }
  }


    renderPage = () =>{
      return this.state.data.accounts.data.map(t=>{
        return (
          <View key={t.name}>
            <Card>
                <CardItem>
                    <Body style={{ flex: 1, flexDirection: 'row' }}>
                    <Image
                     source={{uri: t.photoURL}}
                     style={styles.profileImage}
                     resizeMode='contain'
                   />
                    <View style={{ paddingLeft:15 }}>
                    <Text>
                        Page Name
                    </Text>
                    <Text style={{fontSize:20,fontWeight:'bold'}}>
                        {t.name}
                    </Text>

                    <TouchableOpacity key={t} style={styles.buttonStyle}
                    onPress={()=>{this.getLive(this.state.token,t)}}>
                      <Text
                      style={styles.textStyle}
                      >connect</Text>
                  </TouchableOpacity>
                    </View>
                    </Body>
                </CardItem>
            </Card>
        </View>
        )
      })
    }


  read = async () => {
  try {
    const id = await SecureStore.getItemAsync('id');

    if (id) {
      const myJson = JSON.parse(id);
      this.setState({
        name: myJson.name,
        photoURL: myJson.photoURL,
        token: myJson.token,
      });
        this.callGraph(this.state.token);
    }
  } catch (e) {
    console.log(e);
  }
  };









  render(){
    return (
      <View style={{flex:1}}>
        <View style={styles.profileContainer}>
        <Image
         source={{uri: this.state.photoURL}}
         style={styles.profileImage}
         resizeMode='contain'
       />
       <Text style={styles.profileText}>{this.state.name}</Text>
        </View>
        <View style={styles.orderContainer}>

        <ScrollView style={{ paddingTop:15 }}>
        {this.renderPage()}
        </ScrollView>
            </View>
      </View>
    )
  }
}



/* for sign out
<View style={styles.orderContainer}>
  <Button style={styles.buttonStyle}
    onPress={()=> firebase.auth().signOut()}
  >
    <Text style={styles.textStyle}>Sign Out</Text>
  </Button>
</View>*/




const Tab = createBottomTabNavigator({
  Profile:{
    screen:ProfileScreen,
    },
  Live:{
    screen:LiveScreen,
    },
  Shop:{
      screen:ShopScreen,
      },
  },
  {
    initialRouteName: 'Live',
    order:['Live','Shop','Profile'],
    defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({focused,tintColor}) => {
            const { routeName } = navigation.state;
            let iconName;
            if (routeName === 'Profile') {
              iconName = focused ? 'ios-contact' : 'ios-contact';
            } else if (routeName === 'Shop') {
              iconName = focused ? 'ios-cart' : 'ios-cart';
            }
            else if (routeName === 'Live') {
              iconName = focused ? 'ios-play-circle' : 'ios-play-circle';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={25} color={tintColor} style={{paddingBottom:1,paddingTop:8}}/>;
          },
        }),
    tabBarOptions: {
      activeTintColor:'#5b64d9',
      inactiveTintColor:'#000',
      style:{
        height:55,
      },
      labelStyle:{
        fontSize: 10,
        fontWeight:'bold',
      },
    },
  }
);
const navigator = createAppContainer(Tab);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent:'center'
  },
  profileContainer: {
    flex: 2,
    backgroundColor: '#5b64d9',
    alignItems: 'center',
    justifyContent:'center'
  },
  profileImage: {
    width:70,
    height:70,
    borderRadius:50,
    borderColor:'white',
    borderWidth:2
  },
  profileText: {
    color:'white',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop:15
  },
  orderContainer: {
    flex: 4,
    backgroundColor: '#fff',
  },
  buttonStyle:{
    width:100,
    height:40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5b64d9',
    borderColor:'white',
    borderWidth: 1,
    borderRadius:20,
    marginTop:10
  },
  liveButtonStyle:{
    width:100,
    height:40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5b64d9',
    borderColor:'white',
    borderWidth: 1,
    borderRadius:20,
    marginTop:10,
    position:'absolute',
    left:'40%',
    bottom:'2%',
  },
  textStyle:{
    color:'white',
    fontSize: 18
  }
});
export default navigator;
