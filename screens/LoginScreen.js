import React from 'react';
import { StyleSheet, Text,Image } from 'react-native';
import * as Facebook from 'expo-facebook';
import {Container,Item,Button,Icon  } from 'native-base'
import * as firebase from 'firebase';
import * as SecureStore from 'expo-secure-store';

class LoginScreen extends React.Component{



  async logInWithFacebook() {
    Facebook.initializeAsync('2380971085340047','todo')
    const {
      type,
      token
    } = await Facebook.logInWithReadPermissionsAsync
    ('2380971085340047',{ permissions: ['public_profile','pages_show_list','manage_pages','pages_messaging','Page Public Content Access']})

    if (type === 'success') {
      const credential = firebase.auth.FacebookAuthProvider.credential(token)
      firebase
      .auth()
      .signInWithCredential(credential)
      .then((credential) => {
          const name = credential.displayName;
          const photoURL = credential.photoURL+"?&type=large";
          const credentials = { name, photoURL, token};
          try {
          SecureStore.setItemAsync('id', JSON.stringify(credentials));
          }
          catch (e) {
            console.log(e);
          }
          firebase
            .database()
            .ref('/seller/'+credential.displayName)
            .set({
              uid:credential.uid,
              profile_picture: photoURL,
              email: credential.email,
              last_logged_in: Date.now(),
              created_at: Date.now()
            })
      })
      .catch((error) =>{
        console.log(error)
      })
  }
}



  render(){
    return (
      <Container style={styles.container}>
        <Image
        style={{ width: 300, height: 300 }}
        resizeMode='contain'
        source={require('./../assets/login.jpg')}
      />
      <Text style={{fontSize:30,fontWeight:'bold'}}>ANONTZ</Text>
      <Text style={{fontSize:15,textAlign:'center',marginLeft:40,marginTop:20,marginRight:40}}>Shop millions of products online through millions of live streaming from vendors in Cambodia</Text>
          <Button style={styles.buttonStyle}
            onPress={()=> this.logInWithFacebook()}
          >
             <Icon type="FontAwesome" name="facebook" style={{fontSize:23,marginLeft:-10}}/>
            <Text style={styles.textStyle}>Login with Facebook</Text>
          </Button>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop:70
  },
  buttonStyle:{
    width:300,
    height:50,
    justifyContent: 'center',
    backgroundColor: '#5b64d9',
    borderColor:'white',
    borderWidth: 1,
    borderRadius:25,
    marginTop:30
  },
  textStyle:{
    color:'white',
    fontSize: 18
  }
});

export default LoginScreen;
