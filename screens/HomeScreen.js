import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
/**
 * - AppSwitchNavigator
 *    - WelcomeScreen
 *      - Login Button
 *      - Sign Up Button
 *    - AppDrawerNavigator
 *          - Dashboard - DashboardStackNavigator(needed for header and to change the header based on the                     tab)
 *            - DashboardTabNavigator
 *              - Tab 1 - FeedStack
 *              - Tab 2 - ProfileStack
 *              - Tab 3 - SettingsStack
 *            - Any files you don't want to be a part of the Tab Navigator can go here.
 */

import {
  createSwitchNavigator,
  createAppContainer,
  createDrawerNavigator,
  createBottomTabNavigator,
  createStackNavigator
} from 'react-navigation';
class App extends Component {
  render() {
    return <AppContainer />;
  }
}
export default App;



class DashboardScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>DashboardScreen</Text>
      </View>
    );
  }
}

class Order extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button title="Go To Detail Screen" onPress={() => this.props.navigation.navigate('Detail')} />
      </View>
    );
  }
}

class Live extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Live</Text>
      </View>
    );
  }
}

class Product extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Product</Text>
      </View>
    );
  }
}

const Detail = props => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text>Detail</Text>
  </View>
);

const OrderStack = createStackNavigator(
  {
    Order: {
      screen: Order,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: 'Order',
          headerLeft: (
            <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
          )
        };
      }
    },
    Detail: {
      screen: Detail
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const ProductStack = createStackNavigator({
  Product: {
    screen: Product,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Product',
        headerLeft: (
          <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
        )
      };
    }
  }
});
const LiveStack = createStackNavigator({
  Live: {
    screen: Live,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Live',
        headerLeft: (
          <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
        )
      };
    }
  }
});

const DashboardTabNavigator = createBottomTabNavigator(
  {
    OrderStack:{
    screen:OrderStack,
    navigationOptions: {
      tabBarLabel:"Order",
      tabBarIcon: ({ tintColor }) => (
        <Ionicons name='ios-cash' size={25} color={tintColor} style={{paddingBottom:1,paddingTop:8}}/>
      )
    },
  },
    ProductStack:{
    screen:ProductStack,
    navigationOptions: {
      tabBarLabel:"Product",
      tabBarIcon: ({ tintColor }) => (
        <Ionicons name='ios-cart' size={25} color={tintColor} style={{paddingBottom:1,paddingTop:8}}/>
      )
    },
  },
    LiveStack:{
    screen:LiveStack,
    navigationOptions: {
      tabBarLabel:"Live",
      tabBarIcon: ({ tintColor }) => (
        <Ionicons name='ios-play-circle' size={25} color={tintColor} style={{paddingBottom:1,paddingTop:8}}/>
      )
    },
  }
  },
  {
    initialRouteName: 'OrderStack',
    order:['OrderStack','ProductStack','LiveStack'],
    navigationOptions: ({ navigation }) => ({
          header:null,
        }),
    tabBarOptions: {
      activeTintColor:'#5b64d9',
      inactiveTintColor:'#000',
      showIcon:true,
      style:{
        height:55,
      },
      labelStyle:{
        fontSize: 10,
        fontWeight:'bold',
      },
    },
  }
);
const DashboardStackNavigator = createStackNavigator(
  {
    DashboardTabNavigator: DashboardTabNavigator
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerLeft: (
          <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
        )
      };
    }
  }
);

const AppDrawerNavigator = createDrawerNavigator({
  Dashboard: {
    screen: DashboardStackNavigator
  }
});


const AppContainer = createAppContainer(AppDrawerNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
