import React from 'react';
import { StyleSheet, Text, View,ActivityIndicator } from 'react-native';
import * as firebase from 'firebase';
import * as SecureStore from 'expo-secure-store';


class LoadingScreen extends React.Component{

  componentDidMount(){
    this.checkIfLoggedIn();
  }


checkIfLoggedIn = () =>{
  firebase.auth().onAuthStateChanged((user)=>{
    if(user){
      this.props.navigation.navigate('ProfileScreen');
    }
    else{
      this.props.navigation.navigate('LoginScreen');
    }
});
}

  render(){
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large"/>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  }
});
export default LoadingScreen;
